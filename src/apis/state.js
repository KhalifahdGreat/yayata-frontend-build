import axios from "axios";
// eslint-disable-next-line
import config from "../config";

const instance = axios.create({
   // Just change in the config file only for Dev and Production env
      baseURL: config.API_URL
    
});

instance.interceptors.request.use(async (config) => {
    const token = localStorage.getItem('token');
    config.headers.Authorization = (token ? `Bearer ${token}` : '');
    config.headers.ContentType = 'application/json';
    return config;
});


export const fetchStates = async ()=> {
    const response = await instance.get("states/all")
    return response; 
}

export const createStates = async (payload)=> {
    const response = await  instance.post("states/nw", payload, {
        headers:{
            ContentType:"multipart/form-data"
        }
    })
    return response;
}

export const fetchState = async (stateId)=> {
    const response = await instance.get(`state/${stateId}`)
    return response; 
}