import axios from "axios";
// eslint-disable-next-line
import config from "../config";

const instance = axios.create({
   // Just change in the config file only for Dev and Production env
      baseURL: config.API_URL
    
});

instance.interceptors.request.use(async (config) => {
    const token = localStorage.getItem('token');
    config.headers.Authorization = (token ? `Bearer ${token}` : '');
    config.headers.ContentType = 'application/json';
    return config;
});


export const uploadResult =  async (formData)=> {
    const response = await instance.post("result/upload", formData, {
        headers:{
            ContentType:"multipart/form-data"
        }
    })

    return response;
}

export const fetchStateDetails =  async (stateId)=> {
    const response = await instance.get(`state/${stateId}`)
    return response;
}