import axios from "axios";
// eslint-disable-next-line
import config from "../config";

const instance = axios.create({
   // Just change in the config file only for Dev and Production env
      baseURL: config.API_URL
    
});

instance.interceptors.request.use(async (config) => {
    const token = localStorage.getItem('token');
    config.headers.Authorization = (token ? `Bearer ${token}` : '');
    config.headers.ContentType = 'application/json';
    return config;
});



export const AdminLogin  = async (payload)=> {
    const response =  await instance.post("admin/login", {...payload})
    return response;
}

export const AdhocLogin  = async (payload)=> {
    const response =  await instance.post("adhoc/login", {...payload})
    return response;
}

export const CreateAdmin  = async (payload)=> {
    const response =  await instance.post("admin/register", {...payload})
    return response;
}


export const CreateBulkAdhoc = async (payload)=> {
    const response = await  instance.post("adhoc/register/bulk", payload, {
        headers:{
            ContentType:"multipart/form-data"
        }
    })
    return response;
}

export const GetAllAdhoc = async (query)=> {
    const response = await  instance.get(`adhoc/all${query}`)
    return response;
}

export const RemoveAdhocByIDs = async (ids)=> {
   
    const response = await  instance.put(`adhoc`,{ids})
    return response;
}



export const CreateStates = async (payload)=> {
    const response = await  instance.post("states/nw", payload, {
        headers:{
            ContentType:"multipart/form-data"
        }
    })
    return response;
}

export const GetAdhoc = async (adhocId)=> {
    const response = await  instance.get(`adhoc/${adhocId}`)
    return response;
}
