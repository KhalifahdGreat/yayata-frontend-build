import check from "../svgs/check.svg";

export default function UploadSuccess() {
   return (
      // Upload success (u-s)
      <div className="u-s">
         <div className="container">
            <div className="row justify-content-center">
               <div className="col-lg-8 col-sm-10 col-md-12 d-flex justify-content-center">
                  <div className="wrap w-100">
                     <div className="success-con d-flex justify-content-center">
                        <div className="wrap bg-white rounded-circle text-center">
                           <i>
                              <img src={check} alt="" />
                           </i>{" "}
                           <br />
                           <small>Upload Successful</small>
                        </div>
                     </div>
                     <div className="d-flex justify-content-between ">
                        <button className="btn flex-item inec-bg-pri fw-bold px-4 inec-white">
                           Upload result
                        </button>
                        <button className="btn flex-item inec-bg-pri fw-bold px-4 inec-white">
                           Upload result
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}
