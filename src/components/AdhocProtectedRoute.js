import { useNavigate } from "react-router-dom";

const  AdhocProtectedRoute = ({children})=> {
    const token =  localStorage.getItem("token");
    const navigate = useNavigate()

    if (!token) {
      navigate("/auth/adhoc/signin")
      return 
    }

    return children
} 


export default AdhocProtectedRoute;
