const PageNotFound = ()=>  {
    return (
        <div className="h-[100vh]  w-full flex items-center justify-center">
                <p className="text-black text-lg">Ooops, you landed on the wrong page. 
                    <a className="text-blue-300 ml-2 underline" href="/">Back</a>
                </p>
        </div>
    )
}


export default PageNotFound;
 