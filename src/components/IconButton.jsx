const IconButton = ({children, ...other})=>{
    return (
        <button {...other} title="close" className="hover:bg-[rgba(0,0,0,0.2)] transition-all duration-500 active:opacity-20  rounded-full px-2 py-2 flex items-center justify-center">
            {children}
        </button>
    )
}

export default IconButton;
