import { useContext, useRef, useState } from "react";
import { AdminContext } from "../context/adminDataContext";
import upload from "../svgs/upload.svg";

const FileUploader = ({title,accept}) => {
   const [selectedFiles, setSelectedFiles] = useState([]);
   const fileInputRef = useRef(null);

   const { dispatch } = useContext(AdminContext);

   useState(() => {
      // if (selectedFiles) {
      // }
   }, [selectedFiles]);

   const handleFileSelect = (e) => {
      //const files = Array.from(e.target.files);
      const file = e.target.files[0] 
      setSelectedFiles(file);
      dispatch({ type: "FILES", payload: file });
      // console.log(state)
   };

   const handleDrop = (e) => {
      e.preventDefault();
      //const files = Array.from(e.dataTransfer.files);
      const file = e.dataTransfer.files[0]
      setSelectedFiles(file);
      dispatch({ type: "FILES", payload: file });
   };

   const handleDragOver = (e) => {
      e.preventDefault();
   };
   const handleButtonClick = (e) => {
      // Trigger the file selection dialog when the button is clicked
      e.preventDefault();
      fileInputRef.current.click();
   };

   return (
      // Drag And Drop Zone
      <div
         style={{
            border: "2px dashed #ccc",
            borderRadius: "20px",
            padding: "20px",
            textAlign: "center",
            minHeight: "100px",
         }}
         className="text-center"
         onDrop={handleDrop}
         onDragOver={handleDragOver}
      >
         <div className="img-con">
            <img src={upload} alt="upload" className="mb-2" /> <br />
            <span className="inec-white h4">Drag & Drop your files here</span>
            <br /> <br />
            <small className="inec-smoke h5">OR</small>
            <br /> <br />
         </div>

         <div className="btn-con mb-4">
            <button
               className="btn inec-bg-pri fw-bold px-4 inec-white"
               onClick={handleButtonClick}
            >
               Upload {title}
            </button>
         </div>
         <input
            type="file"
            multiple
            onChange={handleFileSelect}
            ref={fileInputRef}
            className="d-none"
            accept={accept}
         />
         {/* {selectedFiles.length > 0 && (
            <div className="text-light">
               <small className="fw-bold fx h6 mt-3">Selected Files:</small>
               <ul>
                  {selectedFiles.map((file) => (
                     <li key={file.name}>{file.name}</li>
                  ))}
                  
               </ul>
            </div>
         )} */}
         <div className="text-light">
               <small className="fw-bold fx h6 mt-3">Selected Files:</small>
               <ul>
                  {/* {selectedFiles.map((file) => (
                     <li key={file.name}>{file.name}</li>
                  ))} */}
                   <li key={selectedFiles.name}>{selectedFiles.name}</li>
               </ul>
            </div>
      </div>
   );
};

export default FileUploader;
