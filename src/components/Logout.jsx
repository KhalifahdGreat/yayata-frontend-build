import IconButton from "./IconButton";
import LogoutIcon from "../assets/icons/logout_icon.svg";

const Logout = ()=> {
    const logoutHandler =  ()=>{
           localStorage.removeItem("token")
           localStorage.removeItem("user")
           let accountType = localStorage.getItem("__accountType");
           let loginRoute =  accountType === "admin" ? "/auth/admin/signin" : "/auth/adhoc/signin" 
           window.location.href=loginRoute
    }

    return (
     <IconButton onClick={logoutHandler}>
        <img src={LogoutIcon} alt=""/>
        <p className="ml-2">Logout</p>
     </IconButton>
    )
}

export default Logout;
