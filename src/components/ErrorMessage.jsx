const ErrorMessage = ({message, className})=> {
    console.log(message)
    return (
        <div className={`text-red-400 text-sm ${className}`}>
            {message}
        </div>
    )
}


export default ErrorMessage