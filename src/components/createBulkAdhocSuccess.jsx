import ReactModal from "react-modal";
import SuccessIcon from "../assets/vectors/success.svg";
import CsvFileDownload from "./CsvFileDownload";
import CloseIcon from "../assets/icons/close.svg";
import IconButton from "./IconButton";

const CreateBulkAdhocModal  = ({isOpen, setOpen, csvData, fileName, successMessage})=> {

    return (
        <ReactModal isOpen={isOpen}
            style={{
                content:{
                    backgroundColor:"rgb(255,255,255)",
                    display:"flex",
                    flexDirection:"column",
                    alignItems:"center",
                    //width:"500px",
                    margin:"auto"
                },
                overlay: {
                    backgroundColor:"rgba(0,0,0,0.7)"
                }
            }}
            
            className="w-full h-full bg-transparent flex items-center justify-center focus:outline-none"
           
        >
            <div className="relative w-[80%] transition-all  md:w-[600px] h-[80vh] focus:outline-none bg-white rounded-lg flex flex-col items-center justify-center">
                <div className=" absolute top-[10px] right-[10px] md:top-[20px] md:right-[20px]">
                    <IconButton onClick={()=>setOpen(false)}>
                        <img width={"30px"} height={"30px"} src={CloseIcon} alt=""/>
                    </IconButton>
                </div>
                <div className="absolute flex justify-center  top-[50px]  w-full  px-[15px]">
                    <h1 className="text-center  text-sm md:text-lg font-bold">{successMessage}</h1>
                </div>
                
                <img className="w-[150px] h-[150px] md:w-[200px] md:h-[200px]" src={SuccessIcon} alt="Success"/>
                {
               
               <div className="mt-5 px-20">
                  <CsvFileDownload csvData={csvData} fileName={fileName} />
               </div>
                }
               
            </div>
        </ReactModal>
    )
}

export default CreateBulkAdhocModal;
