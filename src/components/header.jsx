import React from "react";

export default function Header({title}) {
   return (
      <div className="inec-bg-pri py-2 text-center font-bold inec-white">
         <span>{title}</span>
         
      </div>
   );
}
