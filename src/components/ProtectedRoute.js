import { useNavigate } from "react-router-dom";

const  AdminProtectedRoute = ({children})=> {
    const token =  localStorage.getItem("token");
    const navigate = useNavigate()

    if (!token) {
      window.location.href="/auth/admin/signin"
    }

    return children
} 


export default AdminProtectedRoute;
