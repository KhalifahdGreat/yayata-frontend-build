function CsvFileDownload({ csvData, fileName }) {
    const handleDownload = () => {
      const csvBlob = new Blob([csvData], { type: 'text/csv' });
      const csvUrl = URL.createObjectURL(csvBlob);
      const link = document.createElement('a');
      link.href = csvUrl;
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    };
  
    return (
      <button className="bg-yayataGreen text-white font-bold hover:opacity-80 rounded-[10px] px-4 py-2" onClick={handleDownload}>Download details</button>
    );
  }
  
  export default CsvFileDownload;