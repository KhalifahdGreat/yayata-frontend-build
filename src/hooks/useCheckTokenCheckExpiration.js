import parseJwt from "../utils/parseJwt";
// eslint-disable-next-line
import React, {useEffect} from "react";

const useCheckTokenExpiration =  (setUser)=> {

    useEffect(()=>{
        const intervalId =  setInterval(() => {
          checkTokenExpiration();
        }, 60000);
        
        return () => {
          clearInterval(intervalId)
        }
      })
    
      const checkTokenExpiration = () => {
       const token = localStorage.getItem('token');
       if (token) {
         const decodedToken = parseJwt(token)
         if (decodedToken?.exp < Date.now() / 1000) {
           localStorage.removeItem("token")
           localStorage.removeItem("user")
           let accountType = localStorage.getItem("__accountType");
           let loginRoute =  accountType === "admin" ? "/auth/admin/signin" : "/auth/adhoc/signin" 
           window.location.href=loginRoute
         }
       }
     };
}


export default useCheckTokenExpiration;
