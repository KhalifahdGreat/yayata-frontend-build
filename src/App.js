import logo from "./logo.svg";
import "./App.css";
import "./css/auth.css";
import "./css/adminPages.css";
import { Routes, Route, Router } from "react-router-dom";
import Signin from "./pages/admin/auth/signin";
import SelectState from "./pages/admin/selectState";
import UploadResult from "./pages/admin/uploadResult";
import UploadFile from "./pages/admin/uploadFile";
import View from "./pages/client/view";
import ElectionType from "./pages/client/electionType";
import SelectStates from "./pages/client/selectStates";
import ViewResult from "./pages/client/viewResult";
import AdminDataContext from "./context/adminDataContext";
import ClientRequestContext from "./context/clientRequestContext";
import AdminSignUp from "./pages/admin/auth/signup";
import 'react-toastify/dist/ReactToastify.css';
import {UserContext} from "./context/UserContext";
import { useState, useEffect } from "react";
import Dashboard from "./pages/admin/dashboard";
import AdminProtectedRoute from "./components/ProtectedRoute";
import CreateBulkAdhoc from "./pages/admin/createBulkAdhoc";
import useCheckTokenExpiration from "./hooks/useCheckTokenCheckExpiration";
import AdhocLogin from "./pages/adhoc/auth/AdhocLogin";
import AdhocProtectedRoute from "./components/AdhocProtectedRoute";
import PageNotFound from "./components/PageNotFound";
import Logo from "./assets/logo.png"
import RemoveBulkAdhoc from "./pages/admin/removeAdhoc";
import CreateStates from "./pages/admin/createStates";
import { ToastContainer } from "react-toastify";
import AdminViewResult from "./pages/admin/viewresult/viewResult";
import AdminElectionType from "./pages/admin/viewresult/electionType";
import AdminSelectStates from "./pages/admin/viewresult/selectStates";
import ViewAdhoc from "./pages/admin/viewAdhoc";
import AdhocSelectState from "./pages/adhoc/selectState";
import AdhocUploadResult from "./pages/adhoc/uploadResult";
import AdhocUploadFile from "./pages/adhoc/uploadFile";
import AdhocDashboard from "./pages/adhoc/dashboard";
import AdhocElectionType from "./pages/adhoc/viewresult/electionType";
import AdhocSelectStates from "./pages/adhoc/viewresult/selectStates";
import AdhocViewResult from "./pages/adhoc/viewresult/viewResult";

function App() {
   const [user, setUser]  = useState({})
   const [loading, setLoading] = useState(false)
   useEffect(()=>{
      const user = localStorage.getItem("user")
      if (user) {
         setUser(JSON.parse(user))
      }  
   },[])

   useEffect(()=>{
      setLoading(true)
      setTimeout(()=>{
         setLoading(false)
      },2000)
   },[])
   useCheckTokenExpiration(setUser)
   return (
      <UserContext.Provider value={{user, setUser}}>
         <ClientRequestContext>
         <AdminDataContext>
            <div>
               {
                  loading? <div className="h-[100vh] w-full flex items-center justify-center animate-ping">
                     <img src={Logo} width={100} height={100}/>
                  </div> :
                  <Routes>
                  {/* Admin Routes */}
                  <Route path="/" element={<View />} />
                  <Route path="/auth/adhoc/signin" element={<AdhocLogin/>} />
                  <Route path="/auth/admin/signin" element={<Signin />} />
                  <Route path="/auth/admin/signUp" element={<AdminSignUp />} />
                  <Route path="admin/state" element={<SelectState />} />
                  <Route path="/admin/upload-state" element={<AdminProtectedRoute><SelectState /></AdminProtectedRoute>} />
                  <Route path="admin/upload-result" element={<AdminProtectedRoute><UploadResult /></AdminProtectedRoute>} />
                  <Route path="admin/upload-file" element={<AdminProtectedRoute><UploadFile/></AdminProtectedRoute>} />
                  <Route path="admin/dashboard" element={<AdminProtectedRoute><Dashboard/></AdminProtectedRoute>}/>
                  <Route path="/admin/create-bulk-adhoc" element={<AdminProtectedRoute><CreateBulkAdhoc/></AdminProtectedRoute>} />
                  <Route path="/admin/remove-adhoc" element={<AdminProtectedRoute><RemoveBulkAdhoc/></AdminProtectedRoute>}/>
                  <Route path="/admin/view-adhoc" element={<AdminProtectedRoute><ViewAdhoc/></AdminProtectedRoute>}/>
                  <Route path="/admin/create-states" element={<AdminProtectedRoute><CreateStates/></AdminProtectedRoute>} />
                  <Route path="/admin/result/election-type" element={<AdminProtectedRoute><AdminElectionType /></AdminProtectedRoute>} />
                  <Route
                     path="/admin/result/election-state"
                     element={<AdminProtectedRoute><AdminSelectStates /></AdminProtectedRoute>}
                  />
                  <Route path="/admin/result/election-result" element={<AdminProtectedRoute><AdminViewResult /></AdminProtectedRoute>} />
                  {/* Adhoc routes */}
                  <Route path="/adhoc/upload-state" element={<AdhocProtectedRoute><AdhocSelectState /></AdhocProtectedRoute>} />
                  <Route path="adhoc/upload-result" element={<AdhocProtectedRoute><AdhocUploadResult /></AdhocProtectedRoute>} />
                  <Route path="adhoc/upload-file" element={<AdhocProtectedRoute><AdhocUploadFile/></AdhocProtectedRoute>} />
                  <Route path="adhoc/dashboard" element={<AdhocProtectedRoute><AdhocDashboard/></AdhocProtectedRoute>}/>
                  <Route path="/adhoc/result/election-type" element={<AdhocProtectedRoute><AdhocElectionType /></AdhocProtectedRoute>} />
                  <Route
                     path="/adhoc/result/election-state"
                     element={<AdhocProtectedRoute><AdhocSelectStates /></AdhocProtectedRoute>}
                  />
                  <Route path="/adhoc/result/election-result" element={<AdhocProtectedRoute><AdhocViewResult /></AdhocProtectedRoute>} />

                  {/* Client Routes */}
                  <Route path="/client/view" element={<View />} />
                  <Route path="/client/election-type" element={<ElectionType />} />
                  <Route
                     path="/client/election-state"
                     element={<SelectStates />}
                  />
                  <Route path="/client/election-result" element={<ViewResult />} />
                  <Route path="*" element={<PageNotFound/>}/>
               </Routes>
               }
               <ToastContainer></ToastContainer>
            </div>
         </AdminDataContext>
      </ClientRequestContext>
      </UserContext.Provider>
   );
}

export default App;
