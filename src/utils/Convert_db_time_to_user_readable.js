export function convertDbTimeToReadable (db_time) {
    let newDate = new Date(db_time);
    let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    let dateInString = `${months[newDate.getMonth()]} ${newDate.getDate()}, ${newDate.getFullYear()}`
    return dateInString;
}

export function formatTime (isoTime) {
    
        const date = new Date(isoTime);
    
        const options = {
        hour: 'numeric',
        minute: 'numeric',
        //timeZoneName: 'short',
        };
       
        const userReadableDate = date.toLocaleString([], options);
        return userReadableDate;
    
}