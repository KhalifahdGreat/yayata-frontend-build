//this function helps to format time in pm or am 
const timeOfTheDay = (time)=> {
    const splittedTime = time?.split(":")
    let hours =splittedTime[0];
    let minutes =splittedTime[1];
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // Convert midnight (0) to 12
    
    var formattedTime = hours + ':' + (minutes < 10 ? '0' + minutes : minutes) + ' ' + ampm;
    return formattedTime;
}

export default timeOfTheDay;
