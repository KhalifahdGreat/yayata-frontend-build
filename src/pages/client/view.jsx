import { useNavigate } from "react-router-dom";
import Header from "../../components/header";
import right from "../../svgs/right.svg";


export default function View() {
   const navigate = useNavigate()
   return (
      <div className="a-states inec-bg-sec py-4">
         <div className="bg-con ">
            {/* Heading section */}
            <Header title={"Election"} />


            {/* Body section: Image, text and select states */}
            <div className="a-s-body d-flex justify-content-center">
               <div className="wrap inec-white text-center">
                  <img src="/logo.png" alt="inec" className="m-3 icon mx-auto" />
                  <div className="text">
                     <p className="font-bold text-[16px] px-2 md:text-[32px]">
                        INDEPENDENT NATIONAL ELECTORAL COMMISSION
                     </p>
                     <br />
                     <small className="text-[14px] md:text-[24px]">
                        Click the button below to view Election Results
                     </small>
                  </div>
                  <div className="select-states">
                     <div className="btn-con mt-3">
                        <button className="px-4 py-2 btn inec-bg-pri inec-white fw-bold" onClick={() => navigate('/client/election-type')}> View</button>
                     </div>
                  </div>
                  
               </div>
            </div>
           
         </div>
         <footer className="absolute  flex w-full justify-center bottom-[60px] text-white text-center">
                  <span className="font-bold uppercase">yayata</span>: Confidentaility, intergrity, and availability
         </footer>
      </div>
   );
}
