import { useContext, useEffect, useState } from "react";
import FileUploader from "../../components/fileUploader";
import Header from "../../components/header";
import UploadSuccess from "../../components/uploadSuccess";
import { AdminContext } from "../../context/adminDataContext";
import left from "../../svgs/left-w.svg";
import FileSample from "../../assets/states_sample.png"
import { ToastContainer, toast } from "react-toastify";
import CsvFileDownload from "../../components/CsvFileDownload";
import { CircleLoader } from "react-spinners";
import { useNavigate } from "react-router-dom";
import CreateBulkAdhocModal from "../../components/createBulkAdhocSuccess";
import { createStates } from "../../apis/state";


export default function CreateStates() {
   const { state } = useContext(AdminContext);
   const [uploading, setUploading] = useState(false)
   const [data,setData] = useState("")
   const navigate = useNavigate()
   const [openModal, setOpenModal] = useState(false);


  

   // Handle submit queries
   const handleSubmit = (e) => {
      e.preventDefault();
      //console.log(state.election_file)
      //console.log(tate.election_file)
      const formData = new FormData()
      formData.append("states_list",state.election_file)
      setUploading(true)
      createStates(formData).then(
        res=>{
         setUploading(false)
         console.log(res)
         setData(res.data)
         setOpenModal(true)
         toast.success("States created successfully")
        },
        err=>  {
            setUploading(false)
            console.log(err)
            toast.error(err.response.data.msg)
        }
      )
    //   const formData = new FormData()
    //   formData.set("users_list",state.FILES)
    };

    const fileName = 'states.csv';
   return (
      // Admin Upload file (a-uf)
      <div className="a-uf inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Assignment"} />

            {/* Form Title */}
            <div className="form-title-wrap">
               <div className="d-flex py-4 mt-5">
                  <button onClick={()=>navigate(-1)}>
                  <img src={left} alt="back" className="px-4"/>
                  </button>
                  <div className="text inec-white">
                     <span className="h4 fw-bold">Upload File</span> <br />
                     <small className="inec-smoke">
                        Upload a CSV  sheet of 
                        Scan/Upload window
                     </small>
                  </div>
               </div>
               <div className="container flex flex-col md:flex-row space-y-10 md:space-y-0 md:space-x-10  md:items-center">
                  {/* Body Section */}
                  <div className="a-uf-body">
                     <form action="" onSubmit={handleSubmit}>
                        {/* Drag and Drop file section */}
                        <div className="drop d-flex">
                           <FileUploader title="CSV"  accept=".csv"/>
                        </div>

            
                        {/* Upload Button */}
                        {
                           state.election_file &&
                           <div className="btn-con mt-5">
                           <button
                              className="btn inec-bg-pri fw-bold px-4 inec-white"
                              // onClick={checkSuccess}
                              type="submit"
                           >
                              {
                                 uploading ? <CircleLoader color="#fff"/> : "Submit"
                              }
                           </button>
                        </div>
                        }
                     </form>
                  </div>
                  <div>
                    <p className="text-white mb-2">Note: Your excel sheets should contain data in this format</p>
                  <img
                    src={FileSample}
                    alt="Example"
                    className="w-[300px] h-[300px]"
                  />
                  </div>
               </div>
            </div>
            <div className=" w-100 s-wrap mt-5">
               <UploadSuccess />
            </div>
            
         </div>
         <ToastContainer></ToastContainer>
         <CreateBulkAdhocModal isOpen={openModal} csvData={data} 
            fileName={fileName} setOpen={setOpenModal}
            successMessage={<>States uploaded and created successfully!</>}
        />
      </div>
   );
}
