import { useContext, useEffect, useState } from "react";
import FileUploader from "../../components/fileUploader";
import Header from "../../components/header";
import UploadSuccess from "../../components/uploadSuccess";
import { AdminContext } from "../../context/adminDataContext";
import left from "../../svgs/left-w.svg";
import { GetAllAdhoc, RemoveAdhocByIDs, CreateBulkAdhoc as createAdHoc } from "../../apis/User";
import { ToastContainer, toast } from "react-toastify";
import { CircleLoader } from "react-spinners";
import { useNavigate } from "react-router-dom";


import {  TrashIcon } from "@heroicons/react/24/solid";
import {
  
  MagnifyingGlassIcon,
} from "@heroicons/react/24/outline";
import {
  Card,
  CardHeader,
  Typography,
  Button,
  CardBody,
  CardFooter,
  IconButton,
  Tooltip,
  Input,
  Checkbox,
  Dialog,
  DialogBody,
  DialogHeader,
  DialogFooter,
  Spinner,
} from "@material-tailwind/react";
import Skeleton from "react-loading-skeleton";
import ReactModal from "react-modal";
import { convertDbTimeToReadable } from "../../utils/Convert_db_time_to_user_readable";

const TABLE_HEAD = ["Name", "Email", "Date Joined","State of work",  "Account Id", ""];
 

export default function RemoveBulkAdhoc() {
   const [loading, setLoading] = useState(false)
   const [data,setData] = useState([])
   const navigate = useNavigate()
   const [openModal, setOpenModal] = useState(false);
   const [noOfItemToRemove,setNoOfTimeToRemove] = useState(0)
   const [totalNoOfItem, setTotalNoOfItem] = useState(0)
   const [checkAll, setCheckAll] = useState(false);
   const [searchQuery, setSearchQuery] = useState("")
   const [selectedItem, setSelectedItem] = useState([])
   const [openDialog, setOpenDialog] = useState(false)
   const [refresh, setRefresh ] = useState(false)

    useEffect(()=>{
        const fetchAllAdhoc = ()=>{
            setLoading(true)
            GetAllAdhoc(`?filterValue=${searchQuery}`).then(
                res=>{
                    setData(res.data)
                    setTotalNoOfItem(res.data.length)
                    setLoading(false)
                },
                err=> {
                    toast.error("Failed to fetch adhoc!")
                    setLoading(false)
                }
            )
        }

        fetchAllAdhoc()
    },[searchQuery,refresh])

    useEffect(()=>{
         setNoOfTimeToRemove(selectedItem.length)
    },[selectedItem])
   
   // success caller if data is uploaded
   const checkSuccess = () => {
      document.querySelector(".form-title-wrap").classList.toggle("off");
      document.querySelector(".s-wrap").classList.toggle("on");
   };

   const checkNewitem = (id)=>{
    const value = selectedItem.find(selectedItem=>selectedItem===id)
    if (value) {
        const newArray = selectedItem.filter((item) => item !== id);
        setSelectedItem(newArray)
        return
    }
    //add to selectedItem
    setSelectedItem(selectedItem=>[...selectedItem,id])
   }

   const isChecked = (id)=>{
        if (selectedItem.includes(id)) {
            return true
        }

        else {
            return false
        }
     
   }
   const checkAllItem  =  ()=> {
        if (!checkAll) {
            data.map(({_id})=>{
                if (!selectedItem.includes(_id)) {
                    setSelectedItem(selectedItem=>[...selectedItem,_id])
            }
           })
           setCheckAll(true)
        }
        else {
            setSelectedItem([])
            setCheckAll(false)
        }
   }
   // Handle submit queries
//    const handleSubmit = (e) => {
//       e.preventDefault();
//       //console.log(state.election_file)
//       //console.log(tate.election_file)
//       const formData = new FormData()
//       formData.append("users_list",state.election_file)
//       setUploading(true)
//       createAdHoc(formData).then(
//         res=>{
//          setUploading(false)
//          console.log(res)
//          setData(res.data)
//          setOpenModal(true)
//          toast.success("Users created successfully")
//         },
//         err=>  {
//             setUploading(false)
//             console.log(err)
//             toast.error(err.response.data.msg)
//         }
//       )
//     //   const formData = new FormData()
//     //   formData.set("users_list",state.FILES)
//     };

   const handleRemoveByAdhocs =  ()=>{
   
    setOpenDialog(false)
    setOpenModal(true)
    RemoveAdhocByIDs(selectedItem).then(
         res=>{
            toast.success("Adhoc(s) removed sucessful")
            setOpenModal(false)
            setRefresh(refresh=>!refresh)
         },
         err=>{
            toast.error("Error removing adhocs())")
            setOpenModal(false)
         }
    )
   }

    const fileName = 'adhocs.csv';
   return (
      // Admin Upload file (a-uf)
      <div className="a-uf inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Assignment"} />

            {/* Form Title */}
            <div className="form-title-wrap">
               <div className="d-flex py-4 mt-5">
                  <button onClick={()=>navigate(-1)}>
                  <img src={left} alt="back" className="px-4" />
                  </button>
                  <div className="text inec-white">
                     <span className="h4 fw-bold">Remove adhoc</span> <br />
                     <small className="inec-smoke">
                        Remove adhoc account
                     </small>
                  </div>
               </div>
               <div className="container">
                  {/* Body Section */}
                  <div className="a-uf-body">
                  <Card className="h-full w-full">
                    <CardHeader floated={false} shadow={false} className="rounded-none">
                        <div className="mb-4 flex flex-col justify-between gap-8 md:flex-row md:items-center">
                        <div>
                            <Typography variant="h5" color="blue-gray">
                            Adhoc list
                            </Typography>
                            <Typography color="gray" className="mt-1 font-normal">
                            These are details of the adhoc
                            </Typography>
                        </div>
                        <div className="flex w-full shrink-0 gap-2 md:w-max">
                            <div className="w-full md:w-72">
                            <Input
                                label="Search by name or email"
                                icon={<MagnifyingGlassIcon className="h-5 w-5" />}
                                onChange={(evt)=>setSearchQuery(evt.target.value)}
                            />
                            </div>
                            <Button onClick={()=>setOpenDialog(true)} className="bg-yayataGreen flex items-center gap-3" size="sm">
                            <TrashIcon strokeWidth={2} className="h-4 w-4" /> 
                                Remove ({noOfItemToRemove !== 0 && totalNoOfItem===noOfItemToRemove ? "all" : noOfItemToRemove })
                            </Button>
                        </div>
                        </div>
                    </CardHeader>
                    <CardBody className="overflow-scroll px-0">
                        <table className="w-full min-w-max table-auto text-left">
                        <thead>
                            <tr>
                                <th
                                className="border-y border-blue-gray-100 bg-blue-gray-50/50 p-4"
                                >
                                 <Checkbox  color="green" checked={checkAll} onChange={()=>checkAllItem()}  />
                                </th>
                            {TABLE_HEAD.map((head) => (
                                <th
                                key={head}
                                className="border-y border-blue-gray-100 bg-blue-gray-50/50 p-4"
                                >
                                <Typography
                                    variant="small"
                                    color="blue-gray"
                                    className="font-normal leading-none opacity-70"
                                >
                                    {head}
                                </Typography>
                                </th>
                            ))}
                            </tr>
                        </thead>
                        <tbody>
                            {!loading && data?.length>0 && data?.map(
                            (
                                {
                                fullName,
                                _id,
                                createdAt,
                                email,
                                stateOfWork,
                                },
                                index,
                            ) => {
                                const isLast = index === data?.length - 1;
                                const classes = isLast
                                ? "p-4"
                                : "p-4 border-b border-blue-gray-50";
                
                                return (
                                <tr key={_id}>
                                    <td className={classes}>
                                        <Checkbox checked={isChecked(_id)} onChange={()=>checkNewitem(_id)}  color="green"/>
                                    </td>
                                    <td className={classes}>
                                    <div className="flex items-center gap-3">
                                        {/* <Avatar
                                        src={img}
                                        alt={name}
                                        size="md"
                                        className="border border-blue-gray-50 bg-blue-gray-50/50 object-contain p-1"
                                        /> */}
                                        <Typography
                                        variant="small"
                                        color="blue-gray"
                                        className="font-bold"
                                        >
                                        {fullName}
                                        </Typography>
                                    </div>
                                    </td>
                                    <td className={classes}>
                                    <Typography
                                        variant="small"
                                        color="blue-gray"
                                        className="font-normal"
                                    >
                                        {email}
                                    </Typography>
                                    </td>
                                    <td className={classes}>
                                    <Typography
                                        variant="small"
                                        color="blue-gray"
                                        className="font-normal"
                                    >
                                        {convertDbTimeToReadable(createdAt)}
                                    </Typography>
                                    </td>
                                    <td className={classes}>
                                    <div className="w-max">
                                        {/* <Chip
                                        size="sm"
                                        variant="ghost"
                                        value={status}
                                        color={
                                            status === "paid"
                                            ? "green"
                                            : status === "pending"
                                            ? "amber"
                                            : "red"
                                        }
                                        /> */}
                                        <Typography
                                        variant="small"
                                        color="blue-gray"
                                        className="font-normal"
                                    >
                                        {stateOfWork}
                                    </Typography>
                                    </div>
                                    </td>
                                    <td className={classes}>
                                    <div className="flex items-center gap-3">
                                        
                                        <div className="flex flex-col">
                                        <Typography
                                            variant="small"
                                            color="blue-gray"
                                            className="font-normal capitalize"
                                        >
                                            {_id}
                                        </Typography>
                                        {/* <Typography
                                            variant="small"
                                            color="blue-gray"
                                            className="font-normal opacity-70"
                                        >
                                            {expiry}
                                        </Typography> */}
                                        </div>
                                    </div>
                                    </td>
                                    <td className={classes}>
                                    <Tooltip content="Remove Adhoc">
                                        <IconButton variant="text" onClick={()=>{
                                            checkNewitem(_id);
                                            setOpenDialog(true)
                                        }}>
                                        <TrashIcon className="h-4 w-4" />
                                        </IconButton>
                                    </Tooltip>
                                    </td>
                                </tr>
                                );
                            },
                            )}
                            {loading  && [1,2,3,4,5,6].map(
                            (
                                val,
                                index,
                            ) => {
                                const isLast = index === data?.length - 1;
                                const classes = isLast
                                ? "p-4"
                                : "p-4 border-b border-blue-gray-50";
                
                                return (
                                <tr key={index}>
                                    <td className={classes}>
                                        {/* <Checkbox checked={checkAll}  color="green"/> */}
                                        <Skeleton/>
                                    </td>
                                    <td className={classes}>                                    
                                        <Skeleton/>
                                    </td>
                                    <td className={classes}>
                                        <Skeleton/>
                                    </td>
                                    <td className={classes}>
                                   <Skeleton/>
                                    </td>
                                    <td className={classes}>
                                        <Skeleton/>
                                    </td>
                                    <td className={classes}>
                                        <Skeleton/>
                                   
                                    </td>
                                    <td className={classes}>
                                    
                                    </td>
                                </tr>
                                );
                            },
                            )}
                            {/* {
                                !loading && data?.length<=0 &&
                                <div className="h-[100px]">
                                    <div className="absolute w-full bg-black right-[50%] left-[50%] mt-[30px]">
                                        <div className=" !w-full">
                                            <Typography
                                                variant="lead"
                                                color="blue-gray"
                                                className="font-normal text-center"
                                            >
                                                Empty Adhoc list
                                        </Typography>
                                        </div>  
                                    </div>
                                </div>
                                
                            } */}
                        </tbody>
                        </table>
                    </CardBody>
                    <CardFooter className="flex items-center justify-between border-t border-blue-gray-50 p-4">
                        <Button variant="outlined" size="sm">
                        Previous
                        </Button>
                        <div className="flex items-center gap-2">
                        <IconButton variant="outlined" size="sm">
                            1
                        </IconButton>
                        <IconButton variant="text" size="sm">
                            2
                        </IconButton>
                        <IconButton variant="text" size="sm">
                            3
                        </IconButton>
                        <IconButton variant="text" size="sm">
                            ...
                        </IconButton>
                        <IconButton variant="text" size="sm">
                            8
                        </IconButton>
                        <IconButton variant="text" size="sm">
                            9
                        </IconButton>
                        <IconButton variant="text" size="sm">
                            10
                        </IconButton>
                        </div>
                        <Button variant="outlined" size="sm">
                        Next
                        </Button>
                    </CardFooter>
                    </Card>
                  </div>
                 
               </div>
            </div>
            <div className=" w-100 s-wrap mt-5">
               <UploadSuccess />
            </div>
            
         </div>
         <ToastContainer></ToastContainer>
         {/* <Dialog className="bg-white" open={openModal} handler={()=>setOpenModal(!openModal)}>
          <div>
          <CircleLoader color="#6A8264"/>
          </div>
      </Dialog> */}
      <ReactModal isOpen={openModal}
        style={{
            content:{
                backgroundColor:"rgb(255,255,255)",
                display:"flex",
                flexDirection:"column",
                alignItems:"center",
                //width:"500px",
                margin:"auto"
            },
            overlay: {
                backgroundColor:"rgba(0,0,0,0.7)"
            }
        }}
        
        className="w-full h-full bg-transparent flex items-center justify-center focus:outline-none"
      >
        <div className="flex flex-col items-center justify-center">
            <Spinner className="h-10 w-10 text-yayataGreen"/>
            <Typography className="text-white" as="h5">
                Removing adhoc...
            </Typography>
        </div>
      </ReactModal>
      {/* Account delete confirmation dialog */}
      <Dialog open={openDialog} handler={()=>setOpenDialog(!openDialog)}>
        <DialogHeader>Adhoc Removal confirmation</DialogHeader>
        <DialogBody divider>
          Do you still want to procced with removing this (these) selected adhoc(s)? 
          Please note that removing means adhoc is being fired and the action is irreverseable
        </DialogBody>
        <DialogFooter>
          <Button
            variant="text"
            color="red"
            onClick={()=>setOpenDialog(false)}
            className="mr-1"
          >
            <span>No</span>
          </Button>
          <Button variant="gradient" color="green" onClick={handleRemoveByAdhocs}>
            <span>Yes</span>
          </Button>
        </DialogFooter>
      </Dialog>
      </div>
   );
}
