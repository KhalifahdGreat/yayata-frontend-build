import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import Header from "../../components/header";
import { AdminContext } from "../../context/adminDataContext";

export default function UploadResult() {
   const { state, dispatch } = useContext(AdminContext);
   const resultFunc = (title, value) => {
      return { title, value };
   };
   const navigate = useNavigate()
   const resultArr = [
      resultFunc(
         "PRESIDENTIAL ELECTION - 2023-04-09 - NIGERIA",
         "presidential"
      ),
      resultFunc(
         "GOVERNORSHIP ELECTION - 2023-04-09 - NIGERIA",
         "gubernitorial"
      ),
      resultFunc(
         "SENATORIAL ELECTION - 2023-04-09 - NIGERIA",
         "senatorial"
      ),
      resultFunc(
         "HOUSE OF REPRESENTATIVE ELECTION - 2023-04-09 - NIGERIA",
         "house-of-rep"
      ),
      
   ];

   const handleSubmit = (e) => {
      e.preventDefault()
      //navigate('/admin/upload-file')
      navigate('/admin/upload-state')
   };

   return (
      // admin upload result (a-ur)
      <div className="a-ur inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Assignment"} />

            {/* Body section */}
            <div className="container">
               <div className="a-ur-body inec-white">
                  <p className="font-bold">Upload result for:</p>
                  <form action="" onSubmit={handleSubmit}>
                     {resultArr?.map((result, index) => (
                        <div
                           className="form-group d-flex align-items-center"
                           key={index}
                        >
                           <input
                              type="radio"
                              value={result.value}
                              name="election"
                              className="inec-bg-sec"
                              onChange={(e) =>
                                 dispatch({
                                    type: "ElECTION_TYPE",
                                    payload: e.target.value,
                                 })
                              }
                           />
                           <label className="p-3">{result.title}</label>
                        </div>
                     ))}

                     <br />

                     <button className="btn inec-bg-pri px-5 inec-white font-bold ">
                        Continue
                     </button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   );
}
