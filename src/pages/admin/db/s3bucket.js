import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";

const creds = {
   accessKeyId: "AKIA6OSQOPFOZSVXPGP2",
   secretAccessKey: "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",
};

const client = new S3Client({ region: "eu-north-1", credentials: creds });
const bucketName = "sheezeydev-inec-bucket";

// after a file is uploaded to the bucket, it returns the url for the object in the bucket.
export const uploadToBucket = async (file) => {
   // files data
   const params = {
      Bucket: bucketName,
      Key: file.name,
      Body: file,
      ACL: "public-read",
   };

   //    i'm uploading the file here
   const command = new PutObjectCommand(params);
   try {
      // sends file to bucket
      await client.send(command);

      //   object url config section
      const getUrl = `https://${bucketName}.s3.${"eu-north-1"}.amazonaws.com/${file.name}`;
      const objectUrl = getUrl.replace(/\s/g, "+");

      return objectUrl;
   } catch (err) {
      console.error(err);
   }
};
