import React, { useEffect, useState, useContext } from "react";
import { fetchStateDetails } from "../../../apis/result";
import { ClientContext} from "../../../context/clientRequestContext";
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import config from "../../../config";
import {
   Card,
   CardHeader,
   CardBody,
   CardFooter,
   Typography,
   IconButton,
   Button,
 } from "@material-tailwind/react";
import { convertDbTimeToReadable, formatTime } from "../../../utils/Convert_db_time_to_user_readable";
import { GetAdhoc } from "../../../apis/User";
import timeOfTheDay from "../../../utils/timeOfTheDay";
import ReactModal from "react-modal"
import CloseIcon from "../../../assets/icons/close.svg";
import { useNavigate } from "react-router-dom";



export default function AdminViewResult() {
   const [results, setResults] = useState([])
   const [loading, setLoading] = useState(true)
   const [stateName, setStateName] = useState("")
   const [viewResult, setViewResult] = useState("")
   //const [uploadedBy, setUploadedBy] = useState('')
   const {
      state: { election_type, election_state, election_LGA, election_WARD },
      dispatch,
   } = useContext(ClientContext);
   const navigate = useNavigate()

   useEffect(()=>{
      setLoading(true)
      fetchStateDetails(election_state).then(
         res=>{
            setLoading(false)
           
            setResults(res?.data?.state?.results) 
            //setResults([])
            setStateName(res?.data?.state?.name)
         },
         err=> {
            console.log("Error:",err)
            setLoading(false)
         }
      )
   },[])

   
   console.log(results)
   return (
      // View Result
      <div className="v-r inec-bg-sec inec-white">
         <div className="bg-con">
            {/* Header Section */}
            <div className="inec-bg-pri py-2 text-center font-bold inec-white">
               <span>Election</span>
            </div>

            {/* Body Section */}
            <div className="v-r-body">
               <div className="container pt-5">
                  <div className="head">
                     <h2 className="fw-bold">Governorship elections</h2>
                     <span className="h5">
                        {stateName}
                     </span>
                  </div>

                  <div className="result-con flex  text-center p-2 md:p-5">
                     {
                        loading && results?.length<=0  &&
                        <div className="grid grid-cols-1 md:grid-cols-2  xl:grid-cols-3 mt-10 gap-4">
                           {
                              [1,2,3,4].map((item,i)=>{ 
                                 return (<div className="rounded-[20px] w-full md:w-[320px] lg:w-[400px] h-[400px] bg-white" key={i}>
                                      
                                      <Skeleton className="rounded-[10px]" width={300} height={250} />
                                     
                                       <Skeleton width={200}/>
                                       <Skeleton width={150}/>
                                       <Skeleton width={100} height={50}/>
                                 </div>)
                              })
                           }
                        </div>
                     }

                     {
                        !loading && results?.length>0 &&
                        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 mt-10 gap-4">
                          {
                           results.map((res,index)=> {
                              return ( 
                              <Card   key={index} className="w-[100%] md:w-[320px] lg:w-96">
                                 <CardHeader floated={false} className="h-80">
                                 <img src={config.API_URL+"result/single/"+res?._id} alt="result"  />
                                 </CardHeader>
                                 <CardBody className="text-center">
                                 <Typography variant="h4" color="blue-gray" className="mb-2 capitalize">
                                    {res?.resultType}
                                 </Typography>
                                 <Typography color="blue-gray" className="font-normal" textGradient>
                                   Uploaded- {convertDbTimeToReadable(res?.uploadedOn)}
                                 </Typography>
                                 <Typography color="blue-gray" className="font-normal" textGradient>
                                   at- {formatTime(res?.uploadedOn)}
                                 </Typography>
                                 </CardBody>
                                 <CardFooter className="flex justify-center gap-7 pt-2">
                                    <Button onClick={()=>setViewResult(config.API_URL+"result/single/"+res?._id)} className="bg-yayataGreen">
                                       View
                                    </Button>
                                 </CardFooter>
                           </Card>)
                           } )
                          }
                        </div>
                        }
                        {
                           !loading && results?.length<=0 &&
                          <div className="w-full flex justify-center h-full ">
                            <Card className="mt-6 w-96 ">
                           <CardBody>
                          
                           <Typography>
                             No results found for this state
                           </Typography>
                           </CardBody>
                           <CardFooter className="pt-0">
                           <Button className="bg-yayataGreen"
                              onClick={()=>navigate("/admin/upload-result")}
                           >
                                 Upload
                           </Button>
                           </CardFooter>
                        </Card>
                          </div>

                        }
                     {/* <img src="/cake.jpeg" alt="result" className="img-fluid" /> */}
                  </div>
               </div>
            </div>
         </div>
         <ReactModal
                              isOpen={viewResult.length>0}
                              style={{
                                 content:{
                                    backgroundColor:"rgb(255,255,255)",
                                    display:"flex",
                                    flexDirection:"column",
                                    alignItems:"center",
                                    //width:"500px",
                                    margin:"auto"
                                 },
                                 overlay: {
                                    backgroundColor:"rgba(0,0,0,0.7)"
                                 }
                           }}
                           
                           className="w-full h-full bg-transparent flex items-center justify-center focus:outline-none"
                           >
                              <div className="relative w-[80%] transition-all  md:w-[600px] h-[90vh] focus:outline-none bg-white rounded-lg flex flex-col items-center justify-center">
                                 <div className=" absolute top-[10px] right-[10px] md:top-[20px] md:right-[20px]">
                                    <IconButton variant="text" onClick={()=>setViewResult("")}>
                                          <img width={"30px"} height={"30px"} src={CloseIcon} alt=""/>
                                          {/* <XMarkIcon className="text-yayataGreen"/> */}
                                    </IconButton>
                                 </div>
                                 {/* <div className="absolute flex justify-center  top-[50px]  w-full  px-[15px]">
                                    <h1 className="text-center  text-sm md:text-lg font-bold">Image Preview</h1>
                                 </div> */}
                                 
                                 {
                                    viewResult ? 
                                    <img  
                                       className="w-[90%] h-[400px] " 
                                       src={viewResult} 
                                       alt="result"
                                    />:
                                    null
                                 }
                                 <div className="btn-con mt-5">
                                             <button
                                                className="btn inec-bg-pri fw-bold px-4 inec-white"
                                                onClick={()=>null}
                                                type="submit"
                                             >
                                                Download
                                             </button>
                                    </div>
                                 
                              </div>
                           </ReactModal>
      </div>
   );
}
