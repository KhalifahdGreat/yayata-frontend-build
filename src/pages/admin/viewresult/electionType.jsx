import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import Header from "../../../components/header";
import { ClientContext } from "../../../context/clientRequestContext";

export default function AdminElectionType() {
   const resultFunc = (title, value) => {
      return { title, value };
   };
   const { state, dispatch } = useContext(ClientContext);
   const resultArr = [
      resultFunc(
         "PRESIDENTIAL ELECTION - 2023-04-09 - NIGERIA",
         "PRESIDENTIAL ELECTION"
      ),
      
      resultFunc(
         "GOVERNORSHIP ELECTION - 2023-04-09 - NIGERIA",
         "GOVERNORSHIP ELECTION"
      ),
      resultFunc(
         "HOUSE OF REPRESENTATIVE ELECTION - 2023-04-09 - NIGERIA",
         "HOUSE OF REPRESENTATIVE ELECTION"
      ),
      resultFunc(
         "HOUSE OF ASSEMBLY ELECTION - 2023-04-09 - NIGERIA",
         "HOUSE OF ASSEMBLY ELECTION"
      ),
   ];
   
   const navigate = useNavigate()

   const handleSubmit = (e) => {
      e.preventDefault();

      navigate("/admin/result/election-state")

   };

   return (
      // admin upload result (a-ur)
      <div className="a-ur inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Election"} />

            {/* Body section */}
            <div className="container">
               <div className="a-ur-body inec-white">
                  <p className="font-bold">Select Election Type:</p>
                  <form action="" onSubmit={handleSubmit}>
                     {resultArr.map((result, index) => (
                        <div
                           className="form-group d-flex align-items-center"
                           key={index}
                        >
                           <input
                              type="radio"
                              value={result.value}
                              name="election"
                              className="inec-bg-sec"
                              onChange={(e) =>
                                 dispatch({
                                    type: "ELECTION_TYPE",
                                    payload: e.target.value,
                                 })
                              }
                           />
                           <label className="p-3">{result.title}</label>
                        </div>
                     ))}

                     <br />

                     <button
                        type="submit"
                        className="btn inec-bg-pri px-5 inec-white font-bold "
                     >
                        Continue
                     </button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   );
}
