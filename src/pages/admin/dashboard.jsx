import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import Header from "../../components/header";
import { AdminContext } from "../../context/adminDataContext";
import { UserContext } from "../../context/UserContext";
import BulkUsers from "../../assets/vectors/bulk_users.svg";
import RemoveUser from "../../assets/vectors/remove_user.svg";
import Location from "../../assets/vectors/location.svg";
import Team from "../../assets/vectors/team.svg";
import Upload from "../../assets/vectors/upload.svg";
import View from "../../assets/vectors/view.svg";
import Logout from "../../components/Logout";

export default function Dashboard() {
   const { user , setUser } = useContext(UserContext);
   const navigate = useNavigate()
   
   return (
      // admin upload result (a-ur)
      <div className="a-ur inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Admin Dashboard"} />

            {/* Body section */}
            <div className="container flex w-full justify-between items-center">
               <div className="a-ur-body inec-white">
                  <p className="font-bold text-lg">Welcome, <p className="text-sm">{user?.fullName}</p></p>
               </div>
               <div className="a-ur-body inec-white">
                 <Logout/>
               </div>
            </div>
            <div className="!px-20 mt-5">
               <h1 className="text-[16px] leading-[20px] font-bold text-white">What would you like to do?</h1>
            <div className="flex flex-col   md:flex-row md:space-x-5 ">
               <div 
                  onClick={() => navigate('/admin/create-bulk-adhoc')}
                   className="btn-con hover:opacity-80 cursor-pointer group mt-3 w-full h-[150px] bg-white  md:w-[350px] rounded-lg md:h-[250px] flex flex-col items-center justify-center">
                     <img src={BulkUsers} className="w-[60px] h-[60px] md:w-[150px] md:h-[150px]"/>
                     {/* <button className="px-4 py-2 " onClick={() => navigate('/admin/create-bulk-adhoc')}>Create bulk adhoc</button> */}
                     <p className=" mt-3 text-lg group-hover:text-yayataGreen">Create bulk adhoc</p>
               </div>
               <div 
                  onClick={() => navigate('/admin/remove-adhoc')}
                  className="btn-con hover:opacity-80 cursor-pointer group mt-3 w-full h-[150px] bg-white  md:w-[350px] rounded-lg md:h-[250px]  flex flex-col items-center justify-center">
                     <img src={RemoveUser} className="w-[60px] h-[60px] md:w-[150px] md:h-[150px]"/>
                     {/* <button className="px-4 py-2 " onClick={() => navigate('/admin/create-bulk-adhoc')}>Create bulk adhoc</button> */}
                     <p className=" mt-3 text-lg group-hover:text-yayataGreen">Remove adhoc</p>
               </div>
               <div className="btn-con hover:opacity-80 cursor-pointer group mt-3 w-full h-[150px] bg-white  md:w-[350px] rounded-lg md:h-[250px]  flex flex-col items-center justify-center"
                  onClick={() => navigate('/admin/create-states')}
               >
                     <img src={Location} className="w-[60px] h-[60px] md:w-[150px] md:h-[150px]"/>
                     {/* <button className="px-4 py-2 " onClick={() => navigate('/admin/create-bulk-adhoc')}>Create bulk adhoc</button> */}
                     <p className=" mt-3 text-lg group-hover:text-yayataGreen">Create states</p>
               </div>
               {/* <div className="btn-con mt-3">
                        // <button className="px-4 py-2 btn inec-bg-pri inec-white fw-bold" onClick={() => navigate('/client/election-type')}> Remove adhoc</button>
               </div>
               <div className="btn-con mt-3">
                        <button className="px-4 py-2 btn inec-bg-pri inec-white fw-bold" onClick={() => navigate('/client/election-type')}>Create states</button>
               </div> */}
            </div>
            </div>
            <div className="!px-20 mt-5">
               <h1 className="text-[16px] leading-[20px] font-bold text-white">Other:</h1>
            <div className="flex flex-col md:flex-row md:space-x-5 ">
              
               <div className="btn-con hover:opacity-80 cursor-pointer group mt-3 w-full h-[150px] bg-white  md:w-[350px] rounded-lg md:h-[250px]  flex flex-col items-center justify-center"
                   onClick={() => navigate('/admin/upload-result')}
               >
                     <img src={Upload} className="w-[60px] h-[60px] md:w-[150px] md:h-[150px]"/>
                     {/* <button className="px-4 py-2 " onClick={() => navigate('/admin/create-bulk-adhoc')}>Create bulk adhoc</button> */}
                     <p className=" mt-3 text-lg group-hover:text-yayataGreen">Upload result</p>
               </div>
               <div className="btn-con hover:opacity-80 cursor-pointer group mt-3 w-full h-[150px] bg-white  md:w-[350px] rounded-lg md:h-[250px]  flex flex-col items-center justify-center"
                  onClick={() => navigate('/admin/result/election-type')}
               >
                     <img src={View} className="w-[60px] h-[60px] md:w-[150px] md:h-[150px]"/>
                     {/* <button className="px-4 py-2 " onClick={() => navigate('/admin/create-bulk-adhoc')}>Create bulk adhoc</button> */}
                     <p className=" mt-3 text-lg group-hover:text-yayataGreen">View result</p>
               </div>
               <div className="btn-con hover:opacity-80 cursor-pointer group mt-3 w-full h-[150px] bg-white  md:w-[350px] rounded-lg md:h-[250px]  flex flex-col items-center justify-center"
                   onClick={() => navigate('/admin/view-adhoc')}
               >
                     <img src={Team} className="w-[60px] h-[60px] md:w-[150px] md:h-[150px]"/>
                     {/* <button className="px-4 py-2 " onClick={() => navigate('/admin/create-bulk-adhoc')}>Create bulk adhoc</button> */}
                     <p className=" mt-3 text-lg group-hover:text-yayataGreen">View all ad-hoc staff</p>
               </div>
              
            </div>
            </div>
         </div>
      </div>
   );
}
