import eye from "../../../svgs/eye-slash.svg";
import {Formik} from "formik"
import * as yup from "yup";
import { AdminLogin } from "../../../apis/User";
import { ToastContainer, toast } from "react-toastify";
import {CircleLoader} from "react-spinners"
import ErrorMessage from "../../../components/ErrorMessage";
import React, { useContext, useState } from "react";
import key from "../../../svgs/key.svg";
import person from "../../../svgs/user.svg";
import left from "../../../svgs/left.svg";
import { UserContext } from "../../../context/UserContext";
import { useNavigate } from "react-router-dom";

export default function AdminSignin() {
   const [loading, setLoading] = useState(false);
   const {user, setUser} = useContext(UserContext)
   const navigate = useNavigate()
   const initialValues = {
      fullName:"",
      email:"",
      password:"",
      secret:"",
   }

   const validationSchema = yup.object({ 
      email:yup.string().email("Please provide a valid email address").required("Email address is required"),
      password:yup.string().required("Your password is required"),
   }) 

   const handleSubmit = (values)=>{
      setLoading(true)
      AdminLogin(values).then(
         res=>{
            toast.success(res.data.msg)
            setLoading(false)
            localStorage.setItem("token", res.data.token)
            setUser(res.data.data)
            localStorage.setItem("user", JSON.stringify(res.data.data))
            localStorage.setItem("__accountType","admin")
            //window.location.href="/admin/dashboard"
            navigate("/admin/dashboard")
         },
         err=>{
            console.log("Error",err)
            toast.error(err.response.data.msg)
            setLoading(false)
         }
      )
   }

   return (
      <div className="signin inec-bg-sec">
         <div className="bg-con">
            <div className="container-fluid p-0 text-light">
               <div className="row h-100 p-0 m-0 justify-content-center align-items-center">
                  {/* Logo and Intro section */}
                  <div className="col-lg-6 col-sm-12 col-md-12">
                     <div className="logo-con">
                        <div className=" text-center">
                           <img src="/logo.png" alt="Inec" /> <br />
                           <br />
                           <p className="font-bold">
                              INDEPENDENT NATIONAL ELCETORIAL COMMISSION
                           </p>
                           <small>
                              <i>Result Transmission</i>
                           </small>
                        </div>
                     </div>
                  </div>

               <Formik
                   initialValues={initialValues}
                   validationSchema = {validationSchema}
                   onSubmit={handleSubmit}

               >
                  {
                     ({handleSubmit, handleChange, handleBlur, values, errors, touched })=>(
                        <form className="col-lg-6 col-sm-12 bg-white col-md-12 h-100" onSubmit={handleSubmit}>
                            {/* Form Section */}
                              <div className="navigate px-2 py-4">
                                 <img src={left} alt="back" />
                              </div>
                              <div className="form px-5">
                                 <div className="form-head inec-pri text-center py-5">
                                    <h3 className="font-bold">Welcome back</h3>
                                    <p>Login to your admin account</p>
                                 </div>
                                 <div className="form-inputs">
                                 <div className="mb-3">
                                   <div className="d-flex  col-12">
                                       <i className="inec-bg-pri p-3">
                                          <img src={person} alt="user" />
                                       </i>
                                       <input 
                                       value={values?.email} 
                                       onChange={handleChange}  
                                       name="email" type="text" 
                                       onBlur={handleBlur}
                                       className="w-100 px-3 text-black" 
                                       placeholder="email"
                                       />

                                    </div>
                                    {errors.email && touched?.email && <ErrorMessage message={errors.email}/>}
                                   </div>
                                    {/* Password input */}
                                    <div className="mb-3">
                                    <div className="d-flex ">
                                       <i className="inec-bg-pri p-3">
                                          <img src={key} alt="key" />
                                       </i>
                                       <input 
                                        className="w-100 px-3 text-black" 
                                        value={values?.password} 
                                        onChange={handleChange}  
                                        name="password" type="text" 
                                        onBlur={handleBlur}
                                        placeholder="password"
                                        />
                                       <img src={eye} alt="eye" className="m-2"/>
                                    </div>
                                    {errors.password && touched?.password && <ErrorMessage message={errors.password}/>}
                                    </div>
                                   
                                  
                                    <div className="btn-con">
                                       <button
                                          type="submit"
                                          className="bg-yayataGreen hover:opacity-80 rounded-[5px] h-[48px]  w-100 inec-bg-pri text-light font-bold"
                                       >
                                          {" "}
                                          { loading ? <CircleLoader color="#fff" /> : "Sign in" }
                                          
                                       </button>
                                    </div>
                                    <p className="text-black text-center mt-4">Don't have an account? 
                                       <a className="text-blue-500" 
                                       href="/auth/admin/signup">Create</a>
                                    </p>
                                 </div>
                              </div>
                          
                        </form>
                     )
                  }
               </Formik>
               </div>
            </div>
         </div>
         <ToastContainer/>
      </div>
   );
}
