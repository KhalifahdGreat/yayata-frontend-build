import { useContext, useEffect, useRef, useState } from "react";
import FileUploader from "../../components/fileUploader";
import Header from "../../components/header";
import UploadSuccess from "../../components/uploadSuccess";
import { AdminContext } from "../../context/adminDataContext";
import cancel from "../../svgs/cancel.svg";
import left from "../../svgs/left-w.svg";
import { ToastContainer, toast } from "react-toastify";
import { CircleLoader } from "react-spinners";
import { uploadResult } from "../../apis/result";
import { IconButton } from "@material-tailwind/react";
import { useNavigate } from "react-router-dom";
import ReactModal from "react-modal";
import {XMarkIcon} from "@heroicons/react/24/solid"
import CloseIcon from "../../assets/icons/close.svg";

export default function AdhocUploadFile() {
   const { state, dispatch } = useContext(AdminContext);
   const [uploading, setUploading] = useState(false)
   const navigate = useNavigate()
   const [showPreview, setShowPreview] = useState(false)
   const [imagePreview, setImagePreview] = useState("")
   const formRef = useRef()

   useEffect(()=>{
      if (state.election_file) {
         previewImage(state.election_file)
         setShowPreview(true)
      }
   },[state.election_file])

   // success caller if data is uploaded
   const checkSuccess = () => {
      document.querySelector(".form-title-wrap").classList.toggle("off");
      document.querySelector(".s-wrap").classList.toggle("on");
   };

   // Handle submit queries
   const handleSubmit = (e) => {
      e.preventDefault();
      const formData = new FormData()
      formData.append("result",state.election_file)
      formData.append("resultType", state.election_type)
      formData.append("stateId", state.election_state)
      setUploading(true)
      uploadResult(formData).then(
        res=>{
         setUploading(false)
         toast.success(res.data.msg)
         setShowPreview(false)
         dispatch({ type: "FILES", payload: null });
         navigate("/adhoc/dashboard")
        },
        err=>  {
            setUploading(false)
            console.log(err)
            toast.error(err.response.data.msg)
        }
      )
   };

   const handleOpen = () => setShowPreview((cur) => !cur);

   const previewImage = (file)=> {
      if (file) {
         const reader = new FileReader();
 
         reader.onload = function () {
             setImagePreview(reader.result) 
             
         };
 
         reader.readAsDataURL(file);
     } else {
         setImagePreview("")
     }
   }
   return (
      // Admin Upload file (a-uf)
      <div className="a-uf inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Assignment"} />

            {/* Form Title */}
            <div className="form-title-wrap">
               <div className="d-flex py-4 mt-5">
                  <IconButton onClick={()=>navigate(-1)} className="px-4 bg-transparent">
                     <img src={left} alt="back" className="" />
                  </IconButton>
                  <div className="text inec-white">
                     <span className="h4 fw-bold">Upload File</span> <br />
                     <small className="inec-smoke">
                        Upload result sheet (Form EC8A or Form EC40G (PU))
                        Scan/Upload window
                     </small>
                  </div>
               </div>
               <div className="container">
                  {/* Body Section */}
                  <div className="a-uf-body">
                     <form ref={formRef} action="" onSubmit={handleSubmit}>
                        {/* Drag and Drop file section */}
                        <div className="drop d-flex">
                           <FileUploader accept={"image/*"} />
                        </div>

                        {/* Full Name section */}
                        {/* <label className="inec-white fw-bold h5 mt-3">
                           Please enter your name and sign below. You
                           acknowledge that the result is correct and accurate
                        </label>
                        <div className="form-group mt-3 ">
                           <small className="inec-smoke inec-white fw-bold">
                              Enter full name
                           </small>
                           <div className="d-flex">
                              <input
                                 type="text"
                                 className="py-1 px-4"
                                 onChange={(e) =>
                                    dispatch({
                                       type: "FULL_NAME",
                                       payload: e.target.value,
                                    })
                                 }
                              />
                              <img src={cancel} alt="cancel" className="ms-4" />
                           </div>
                        </div> */}

                        {/* Signing Box section */}
                        {/* <div className="form-group mt-5">
                           <small className="inec-smoke inec-white fw-bold">
                              Sign in the box below
                           </small>
                           <div className="d-flex">
                              <input
                                 type="text"
                                 className="py-3 px-4"
                                 onChange={(e) =>
                                    dispatch({
                                       type: "SIGNATURE",
                                       payload: e.target.value,
                                    })
                                 }
                              />
                              <img
                                 src={cancel}
                                 alt="cancel"
                                 className="ms-4"
                                 width={28}
                              />
                           </div>
                        </div> */}

                        {/* Upload Button */}
                        {
                           state?.election_file &&
                           <div className="btn-con mt-5">
                           <button
                              className="btn inec-bg-pri fw-bold px-4 inec-white"
                              // onClick={checkSuccess}
                           >
                              {
                                 uploading ? <CircleLoader color="#fff"/> : "Upload result"
                              }
                           </button>
                        </div>
                         }
                         <ReactModal
                              isOpen={showPreview}
                              style={{
                                 content:{
                                    backgroundColor:"rgb(255,255,255)",
                                    display:"flex",
                                    flexDirection:"column",
                                    alignItems:"center",
                                    //width:"500px",
                                    margin:"auto"
                                 },
                                 overlay: {
                                    backgroundColor:"rgba(0,0,0,0.7)"
                                 }
                           }}
                           
                           className="w-full h-full bg-transparent flex items-center justify-center focus:outline-none"
                           >
                              <div className="relative w-[80%] transition-all  md:w-[600px] h-[90vh] focus:outline-none bg-white rounded-lg flex flex-col items-center justify-center">
                                 <div className=" absolute top-[10px] right-[10px] md:top-[20px] md:right-[20px]">
                                    <IconButton variant="text" onClick={()=>setShowPreview(false)}>
                                          <img width={"30px"} height={"30px"} src={CloseIcon} alt=""/>
                                          {/* <XMarkIcon className="text-yayataGreen"/> */}
                                    </IconButton>
                                 </div>
                                 <div className="absolute flex justify-center  top-[50px]  w-full  px-[15px]">
                                    <h1 className="text-center  text-sm md:text-lg font-bold">Image Preview</h1>
                                 </div>
                                 
                                 {
                                    imagePreview ? 
                                    <img  
                                       className="w-[90%] h-[400px] " 
                                       src={imagePreview} 
                                       alt="Success"
                                    />:
                                    null
                                 }
                                 <div className="btn-con mt-5">
                                             <button
                                                className="btn inec-bg-pri fw-bold px-4 inec-white"
                                                onClick={()=>formRef.current.requestSubmit()}
                                                type="submit"
                                             >
                                                {
                                                   uploading ? <CircleLoader color="#6A8264"/> : "Upload result"
                                                }
                                             </button>
                                    </div>
                                 
                              </div>
                           </ReactModal>
                     </form>
                  </div>
               </div>
            </div>
            <div className=" w-100 s-wrap mt-5">
               <UploadSuccess />
            </div>
         </div>
         
         
      </div>
   );
}
