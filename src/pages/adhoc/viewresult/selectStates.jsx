import React, { useContext, useState, useEffect } from "react";
import Header from "../../../components/header";
import { ClientContext } from "../../../context/clientRequestContext";
import { fetchState, fetchStates } from "../../../apis/state";
import { useNavigate } from "react-router-dom";

export default function AdhocSelectStates() {
   const {
      state: { election_type, election_state, election_LGA, election_WARD },
      dispatch,
   } = useContext(ClientContext);
   const [states, setStates] = useState([]);
   const navigate = useNavigate()
   const [stateName, setStateName] = useState("")
   useEffect(() => {
      fetchStates().then(
         res=> {
            console.log(res)
            setStates(res.data.states)
         },
         err=>console.log("Error fetching states")
      )
   },[]);

   useEffect(()=>{
      const getState = ()=> {
         fetchState(election_state).then(
            res=>setStateName(res.data?.state?.name),
            err=>setStateName(election_state)
         )
      } 
      getState()
   },[election_state])

   // const LGA_Array = states?.filter((i) => {
   //    return i._id === election_state;
   // });
   // const LGA = LGA_Array[0]?.state?.locals;

   return (
      // Select States
      <div className="s-s inec-bg-sec py-4">
         <div className="bg-con">
            {/* Header Section */}
            <Header title={"Election"} />

            {/* Body section */}
            <div className="s-s-body inec-white">
               <div className="container">
                  <div className="election-data fw-bold py-3">
                     <h4 className="fw-bold">{election_type}</h4>
                     <small>State: {stateName}</small> <br />
                     
                  </div>

                  {/* Form Section */}
                  <div className="form">
                     <div className="form-group text-center mt-5">
                        <label> Select State</label> <br />
                        <select
                           name="states"
                           id="states"
                           className="px-5 py-2 inec-bg-pri inec-white mt-4 rounded"
                           onChange={(e) =>
                              dispatch({
                                 type: "ELECTION_STATE",
                                 payload: e.target.value,
                              })
                           }
                        >
                           <option selected> select a state</option>
                           {states.map((i, index) => (
                              <option value={i._id} key={index}>
                                 {" "}
                                 {i.name}{" "}
                              </option>
                           ))}
                        </select>
                     </div>
                     {/* <div className="form-group text-center mt-5">
                        <label> Select a L.G.A</label> <br />
                        <select
                           name="states"
                           id="states"
                           className="px-5 py-2 inec-bg-pri inec-white mt-4 rounded"
                           onChange={(e) =>
                              dispatch({
                                 type: "ELECTION_LGA",
                                 payload: e.target.value,
                              })
                           }
                        >
                           <option selected> select a L.G.A</option>
                           {LGA?.map((i, index) => (
                              <option value={i.name}>{i.name}</option>
                           ))}
                        </select>
                     </div>
                     <div className="form-group text-center mt-5">
                        <label> Select a WARD</label> <br />
                        <select
                           name="states"
                           id="states"
                           className="px-5 py-2 inec-bg-pri inec-white mt-4 rounded"
                           onChange={(e) =>
                              dispatch({
                                 type: "ELECTION_WARD",
                                 payload: e.target.value,
                              })
                           }
                        >
                           <option selected> select a WARD</option>
                           <option value="ward-tester">slect here</option>
                        </select>
                     </div> */}

                     <div className="btn-con text-center mt-4">
                        <button onClick={()=>navigate("/adhoc/result/election-result")} className="btn inec-bg-smoke px-5">
                           View Result
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}
