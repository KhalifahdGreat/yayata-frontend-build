import { useContext, useEffect, useState } from "react";
import Header from "../../components/header";
import { AdminContext } from "../../context/adminDataContext";
import right from "../../svgs/right.svg";
import {useNavigate} from 'react-router-dom'
import { fetchStates } from "../../apis/state";
import { IconButton } from "@material-tailwind/react";
//import state from "../states.json";

export default function AdhocSelectState() {
   const [states, setStates] = useState([])
   const {
      state: { election_state },
      dispatch,
   } = useContext(AdminContext);
   const [electionState, setElectionState] = useState("");
   useEffect(() => {
      fetchStates().then(
         res=> {
            console.log(res)
            setStates(res.data.states)
         },
         err=>console.log("Error fetching states")
      )
   },[]);

   const navigate = useNavigate()


   const next = () => {
      document.querySelector(".a-s-btn").classList.add("active");
   };

   const handleSubmit = (e) => {
      e.preventDefault();
      dispatch({ type: "STATE", payload: electionState });
      navigate('/adhoc/upload-file')
   };
   return (
      <div className="a-states inec-bg-sec py-4">
         <div className="bg-con">
            {/* Heading section */}
            <Header title={"Result Transmission"} />

            {/* Body section: Image, text and select states */}
            <div className="a-s-body d-flex justify-content-center">
               <div className="wrap inec-white text-center">
                  <img src="/logo.png" alt="inec" className="m-3 icon" />
                  <div className="text">
                     <p className="font-bold">
                        INDEPENDENT NATIONAL ELECTORAL COMMISSION
                     </p>
                     <br />
                     <small>
                        Select the state for result upload of your assignment
                     </small>
                  </div>
                  <form action="" onSubmit={handleSubmit}>
                     <div className="select-states">
                        <select
                           name="states"
                           id="states"
                           className="px-5 py-2 inec-bg-pri inec-white mt-4 rounded"
                           onChange={(e) => {
                              setElectionState(e.target.value);
                              next();
                           }}
                        >
                           <option defaultChecked>Select State</option>
                           {states.map((i, index) => (
                              <option value={i._id}> {i.name} </option>
                           ))}
                        </select>
                     </div>

                     {/* Next Button */}
                     <div className="a-s-btn mt-5">
                        <div className="wrap text-center">
                           <IconButton  type="submit" className="bg-transparent">
                              <img  src={right} alt="next"/>
                           </IconButton>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   );
}
