import { createContext, useReducer } from "react";
import {
   clientRequestReducer,
   CLIENT_REQUEST_INITIAL_STATE,
} from "./clientReducer";

export const ClientContext = createContext();

export default function ClientRequestContext({ children }) {
   const [state, dispatch] = useReducer(
      clientRequestReducer,
      CLIENT_REQUEST_INITIAL_STATE
   );
   return (
      <ClientContext.Provider value={{ state, dispatch }}>
         {children}
      </ClientContext.Provider>
   );
}
