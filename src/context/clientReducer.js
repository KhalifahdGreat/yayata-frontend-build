export const CLIENT_REQUEST_INITIAL_STATE = {
   election_type: "presidential-election",
   election_state: "Abia State",
   election_LGA: "",
   election_WARD: "",
};

export const clientRequestReducer = (state, action) => {
   switch (action.type) {
      case "ELECTION_TYPE":
         return { ...state, election_type: action.payload };
      case "ELECTION_STATE":
         return { ...state, election_state: action.payload };
      case "ELECTION_LGA":
         return { ...state, election_LGA: action.payload };
      case "ELECTION_WARD":
         return { ...state, election_WARD: action.payload };
      default:
         break;
   }
};
