import { createContext, useReducer } from "react";
import { adminDataReducer, ADMIN_DATA_INITIAL_STATE } from "./adminReducer";

export const AdminContext = createContext();

export default function AdminDataContext({ children }) {
   const [state, dispatch] = useReducer(
      adminDataReducer,
      ADMIN_DATA_INITIAL_STATE
   );
   return (
      <AdminContext.Provider value={{ state, dispatch }}>
         {children}
      </AdminContext.Provider>
   );
}
