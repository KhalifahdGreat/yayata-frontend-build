export const ADMIN_DATA_INITIAL_STATE = {
   election_state: "",
   election_type: "",
   election_file: "",
   admin_name: "",
   admin_signature: "",
   resultFile: "",
};

export const adminDataReducer = (state, action) => {
   switch (action.type) {
      case "STATE":
         return { ...state, election_state: action.payload };

      case "ElECTION_TYPE":
         return { ...state, election_type: action.payload };

      case "FILES":
         return { ...state, election_file: action.payload };

      case "FULL_NAME":
         return { ...state, admin_name: action.payload };

      case "SIGNATURE":
         return { ...state, admin_signature: action.payload };
      case "UPLOAD_FILE":
         return { ...state, resultFile: action.payload };
      default:
         break;
   }
};
